const data = ['Regina', 'Napolitaine', 'Spicy'];
// const url = `images/${ name.toLowerCase() }.jpg`;
// const html = 
// `<article class="pizzaThumbnail">
// <a href="${url}">
// <img src="${url}"/>
// <section>Regina</section>
// </a>
// <article class="pizzaThumbnail">`;
let url = '';
let html = '';
let name2 = '';

// for loop
// for(let i = 0; i < data.length; i++ ){
//     name2 = data[i];
//     url = `images/${name2.toLowerCase()}.jpg`;
//     html += `<article class="pizzaThumbnail">
//     <a href="${url}">
//     <img src="${url}"/>
//     <section>${name2}</section>
//     </a>
//     </article\n>`;
// }
// document.querySelector('.pageContent').innerHTML = html;

//forEach loop
// data.forEach(element => {
//     name2 = element;
//     url = `images/${name2.toLowerCase()}.jpg`;
//     html += `<article class="pizzaThumbnail">
//         <a href="${url}">
//         <img src="${url}"/>
//         <section>${name2}</section>
//         </a>
//         </article\n>`;
// });
// document.querySelector('.pageContent').innerHTML = html;

// Map method 
// html = data.map(element => {
//     url = `images/${element.toLowerCase()}.jpg`;
//     return `<article class="pizzaThumbnail">
//         <a href="${url}">
//         <img src="${url}"/>
//         <section>${element}</section>
//         </a>
//         </article\n>`
// });
// document.querySelector('.pageContent').innerHTML = html.join('');

// Reduce Method
const reducer = (previousValue, element) => previousValue + `<article class="pizzaThumbnail">
<a href="images/${element.toLowerCase()}.jpg">
<img src="images/${element.toLowerCase()}.jpg"/>
<section>${element}</section>
</a>
</article\n>`;
document.querySelector('.pageContent').innerHTML = data.reduce(reducer,'');